@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <div class="card-header">{{__('My Account')}}</div>

                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="{{$user->thumbnail?$user->thumbnail:'http://pixselo.com/wp-content/uploads/2018/03/dummy-placeholder-image-400x400-300x300.jpg'}}"/>
                                </div>
                                <div class="col-sm-9">
                                    <h2 class="user-name">{{$user->name}}</h2>
                                    <div class="user-email">{{$user->email}}</div>

                                    <!--actions-->
                                    <div class="actions">
                                        <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary">
                                            {{__('Edit Profile')}}
                                        </a>
                                        <a onclick="return confirm('Are you sure you want to delete your profile?')" href="{{route('user.delete', $user->id)}}" class="btn btn-danger">
                                            {{__('Delete')}}
                                        </a>
                                    </div>
                                    <!--end actions-->
                                </div>

                            </div>
                        </div>



                    <!--posts-->
                    @if(count($user->products))
                        <div class="col-sm-12">
                            <div class="row"><h3 class="col-sm-12">{{__('My Products')}}</h3></div>
                            <div class="actions" style="margin-bottom: 20px">
                                <a href="{{ route('product.add') }}" class="btn btn-primary">{{__('Add New Product')}}</a>
                            </div>
                            @foreach($user->products as $product)
                                <div class="row">
                                    <div class="col-sm-4 post-title"><a href="{{route('product.view',$product->id)}}"><h5 class="post-title">{{$product->title}}</h5></a></div>
                                    <div class="col-sm-4 post-desc">{{str_limit($product->description,50)}}</div>
                                    <!--actions-->
                                    <div class="actions col-sm-4">
                                        <a href="{{ route('product.edit', $product->id) }}" class="btn btn-primary">{{__('Edit Product')}}</a>
                                        <a onclick="return confirm('Are you sure you want to delete this post?')" href="{{route('product.delete', $product->id)}}" class="btn btn-danger">
                                            {{__('Delete')}}
                                        </a>
                                    </div>
                                    <!--end actions-->
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="col-sm-12">
                            <div class="row"><h3 class="col-sm-12">{{__('No Products Yet')}}</h3></div>
                                    <div class="actions" style="margin-bottom: 20px">
                                        <a href="{{ route('product.add') }}" class="btn btn-primary">{{__('Add New Product')}}</a>
                                    </div>
                            <!--end actions-->
                        </div>
                    @endif
                <!--end posts-->
                </div>
            </div>
        </div>
    </div>
@endsection