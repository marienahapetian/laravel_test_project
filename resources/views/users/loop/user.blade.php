<div class="{{$view=='grid'?'col-sm-4':'col-sm-12'}} user-block text-center">
    <a href="{{route('user.view',$user->id)}}">
        <img src="{{$user->getThumbnailPhoto()}}">
    </a>
    <div class="name">
        <a href="{{route('user.view',$user->id)}}">
            {{$user->name}}
        </a>
    </div>
    <div class="email">{{$user->email}}</div>
    <div class="products">
        <a href="{{ route('user.products', $user->id) }}">
            {{ __('Products: ').count($user->products) }}
        </a>
    </div>

    @can('update-user', $user)
        <div class="actions">
            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary">{{__('Edit')}}</a>
            <a onclick="return confirm('Are you sure you want to delete this user?')" href="{{route('user.delete', $user->id)}}" class="btn btn-danger">
                {{__('Delete')}}
            </a>
        </div>
    @endcan
</div>