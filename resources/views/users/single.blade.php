@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Profile #{{$user->id}}</div>

                    <div class="col-sm-12">
                        <div class="clearfix row">
                            <div class="col-sm-3">
                                <img src="{{$user->thumbnail}}"/>
                            </div>
                            <div class="col-sm-9">
                                <h2 class="post-title">{{$user->name}}</h2>
                                <div class="post-content">{{$user->email}}</div>

                                <!--actions-->
                                @if(\Auth::check())
                                    @can('update-user',\Auth::user(),$user )
                                        <div class="row">
                                            <div class="col-md-4">
                                                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary">{{__('Edit')}}</a>
                                                <a onclick="return confirm('Are you sure you want to delete this user?')" href="{{ route('user.delete', $user->id) }}" class="btn btn-danger">
                                                    {{__('Delete')}}
                                                </a>
                                            </div>
                                        </div>
                                    @endcan
                                @endif
                                <!--end actions-->
                            </div>
                        </div>
                    </div>

                    <!--posts-->
                        @if(count($user->products))
                            <div class="col-sm-12 user-posts">
                                <h4 class="col-sm-12">{{__('Products')}}</h4>
                                <div class="row">
                                    @foreach($user->products as $product)
                                        <div class="col-sm-4">
                                            <a href="{{route('product.view',$product->id)}}">
                                                <img src="{{$product->thumbnail}}"/>
                                                <h5 class="post-title">{{$product->title}}</h5>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    <!--end posts-->

                </div>
            </div>
        </div>
    </div>
@endsection
