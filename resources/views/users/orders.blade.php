@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="page-header">{{__('My Orders')}}</div>

                    <div class="card-body">
                        <div class="row table-header">
                            <div class="col-sm-1">
                                {{__('Order ID')}}
                            </div>
                            <div class="col-sm-3">
                                {{__('Transaction')}}
                            </div>
                            <div class="col-sm-2">
                                {{__('Order total')}}
                            </div>
                            <div class="col-sm-3">
                                {{__('Order placed on')}}
                            </div>
                            <div class="col-sm-2">
                                {{__('Status')}}
                            </div>
                            <div class="col-sm-1">
                            </div>
                        </div>

                        @if(!count($orders))
                            {{__('You have no orders placed')}}
                        @else
                            @foreach($orders as $order)
                                <div class="row">
                                    <div class="col-sm-1">
                                        {{ $order->id }}
                                    </div>
                                    <div class="col-sm-3">
                                        {{ $order->transaction_id }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{ $order->totalPrice . $order->currency }}
                                    </div>
                                    <div class="col-sm-3">
                                        {{ $order->created_at }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{ $order->status->name }}
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="{{ route('order.view',$order->id) }}" class="btn btn-info">&rarr;</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
