@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-header">{{__('All Users')}}</div>

                    <div class="card-body">
                        <div class="row">
                            @foreach ($users as $user)
                                @include('users.loop.user',['view'=>'grid'])
                            @endforeach
                        </div>

                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
