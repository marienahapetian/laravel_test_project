@extends('layouts.app')

@section('content')
    <form method="post" action="{{route('user.update',$user->id)}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            @if(session('status'))
                <div class="alert alert-success col-md-8 offset-md-2" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="form-group col-md-6 offset-md-3">
                <label for="name">{{__('Name')}}:</label>
                <input type="text" class="form-control name-input" name="name" value="{{$user->name}}" data-validation="required">
                {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 offset-md-3">
                <label for="email">{{__('Email')}}:</label>
                <input type="text" class="form-control email-input" name="email" value="{{$user->email}}" data-validation="required|email">
                {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6 offset-md-3">
                <label for="thumbnail">{{__('Thumbnail')}}:</label>
                <div class="row file-container">
                    <div class="col-md-3">
                        <img src="{{$user->thumbnail}}"/>
                    </div>
                    <div class="col-md-9">
                        <input type="file" class="form-control file-input" name="thumbnail" value="{{$user->thumbnail}}">
                        <label>{{__('Choose a File')}}</label><br>
                        <span>{{__('or')}}</span><br>
                        <input type="text" name="external-thumb-url" id="external-thumb-url" class="full-width" placeholder=" ex: http://www.birdsandblooms.com/BBam15_RebeccaGranger.jpg">
                        <input type="hidden" name="thumbnail-value" id="thumbnail-value" value="{{$user->thumbnail}}">
                        {!! $errors->first('thumbnail-value', '<p class="error-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                <a href="{{ url()->previous() }}" class="btn btn-cancel">Cancel</a>
            </div>
        </div>
    </form>
@endsection