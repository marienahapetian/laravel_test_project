@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('Seller')}} {{$user->name}}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('error'))
                             <div class="alert alert-danger" role="alert">
                                  {{ session('error') }}
                             </div>
                        @endif


                        @foreach($user->products as $product)
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{$product->thumbnail}}"/>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="col-md-12 title">
                                            <h3>
                                                <a href="{{ route('product.view', $product->id) }}">{{ $product->title }}</a>
                                            </h3>
                                        </div>
                                        <div class="col-md-12"> <h5>{{__('Author')}}: <a href="{{ route('user.view', $user->id) }}">{{ $user->name }}</a> </h5></div>
                                        <div class="col-md-12"> {{ str_limit($product->description,200) }} </div>
                                    </div>


                                    <div class="col-md-3">
                                        @if($canEdit)
                                            <a href="{{ route('product.edit', $product->id) }}" class="btn btn-primary">Edit</a>
                                            <a href="#" class="btn btn-danger">Delete</a>
                                        @endif
                                    </div>
                                </div>
                        @endforeach

                        <a href="{{ url()->previous() }}" class="btn btn-info">&larr; {{__('Back')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
