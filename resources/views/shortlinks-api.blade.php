@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div>
                            <form method="post" action="" class="search-form">
                                @csrf
                                <h3>Get my shortlinks</h3>
                                <h4>Sort by date</h4>
                                <div class="form-group">
                                    <select name="asc-desc">
                                        <option value="asc">{{__('Ascending')}}</option>
                                        <option value="desc">{{__('Descending')}}</option>
                                    </select>
                                </div>
                                <button class="btn btn-info">{{__('Search')}}</button>
                            </form>
                            <div id="results-cont"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function() {
            jQuery('.search-form').on('submit', function (e) {
                debugger;
                e.preventDefault();
                jQuery.ajax({
                    type: "GET",
                    url: 'http://shortly.web/api/link',
                    headers: {"Authorization": "Bearer " + "<?php echo setting('shortly-token');?>"},
                    success: function( response ) {
                        if( response.data && response.data.length ){
                            var results_html = '<table style="width:100%;">';
                            results_html += '<tr>'+
                                '<th>Shortlink</th>'+
                                '<th>Redirect</th>'+
                                '</tr>';

                            jQuery.each(response.data, function(i, item) {
                                results_html += '<tr class="result-item">'+
                                    '<td>'+item.shortlynk+'</td>'+
                                    '<td>'+item.redirect+'</td>'+
                                    '</tr>';
                            });
                            results_html += '</table>';
                        } else {
                            var results_html = 'No Search Results';
                        }

                        jQuery('#results-cont').html(results_html);
                    }
                });
            });
        });
    </script>
@endsection