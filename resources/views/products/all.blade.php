@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <div class="card-header">{{__('All Products')}}</div>

                    <div class="card-body">
                        @if(\Auth::check())
                            @can('publish articles')
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="{{ route('product.add') }}" class="btn btn-success"><i class="far fa-plus"></i> {{__('Add New')}}</a>
                                    </div>
                                </div>
                            @endcan
                        @endif


                        <div class="row">
                            @foreach ($products as $product)
                                @include('products.loop.product')
                            @endforeach
                        </div>
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

