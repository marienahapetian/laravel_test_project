<div class="col-sm-4 post-block">
    <a href="{{route('product.view',$product->id)}}">
        <div class="thumb-cont">
            <img src="{{$product->getThumbnailPhoto()}}"/>
        </div>
        <div class="title">
            {{str_limit($product->title,30)}}
        </div>
        <div class="price">
            {{ $product->price . setting('currency') }}
        </div>
    </a>

    @if(!$product->manage_stock || $product->stock_availability)
        <form method="post" class="add-to-cart" action="{{route('product.addToCart',$product->id)}}">
            @csrf
            <button class="btn btn-primary full-width">{{__('Add to Cart')}}</button>
        </form>
    @else
        <span class="full-width out-of-stock">{{__('Out of Stock')}}</span>
    @endif

    @can('update-product', $product)
        <a href="{{ route('product.edit', $product->id) }}" class="btn  btn-primary"><i class="fas fa-edit"></i></a>
        <a  class="btn  btn-danger" onclick="return confirm('Are you sure you want to delete this post?')" href="{{route('product.delete', $product->id)}}"><i class="fas fa-trash"></i></a>
    @endcan

</div>