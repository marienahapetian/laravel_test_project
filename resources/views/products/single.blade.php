@extends('layouts.app')

@section('content')
    <div class="container single-product">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ url()->previous() }}" class="btn btn-info">&larr; {{__('Back')}}</a>
                        </div>
                        <div class="col-sm-4">
                            <img src="{{$product->thumbnail}}"/>
                        </div>
                        <div class="col-sm-8">
                            <h3 class="post-title">{{$product->title}}</h3>

                            <div class="post-price">
                                {{__('Price')}}: {{$product->price . setting('currency')}}
                            </div>

                            <div class="post-desc">{{$product->description}}</div>

                            @if(!$product->manage_stock || $product->stock_availability)
                                    <form method="post" class="add-to-cart" action="{{route('product.addToCart',$product->id)}}">
                                        @csrf
                                        <label for="quantity"><strong>{{__('Quantity')}}</strong></label>
                                        <input id='quantity' name="quantity" type="number" value="1"  min="1" max="{{$product->manage_stock?$product->stock_availability:''}}" />

                                        <button class="btn btn-primary" style="margin-left: 10px"><i class="fas fa-shopping-cart"></i></button>
                                        @if($product->stock_availability)
                                            <p class="stock in-stock"><i class="fas fa-smile"></i>{{$product->stock_availability.__(' in stock')}} </p>
                                        @endif
                                    </form>
                            @else
                                    <span style="padding: 5px; background: lightgrey">{{__('Out of Stock')}}</span>
                            @endif
                        </div>
                        <div class="col-sm-12 product-content">
                            <h4>{{__('About Product')}}</h4>
                            {!! $product->content !!}
                        </div>
                    </div>

                    <!--actions-->
                    @if($canEdit)
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ route('product.edit', $product->id) }}" class="btn btn-primary">
                                    {{__('Edit')}}
                                </a>
                                <a onclick="return confirm('Are you sure you want to delete this post?')" href="{{route('product.delete', $product->id)}}" class="btn btn-danger">
                                    {{__('Delete')}}
                                </a>
                            </div>
                        </div>
                @endif
                    <!--end actions-->
                </div>
            </div>
        </div>
    </div>
@endsection
