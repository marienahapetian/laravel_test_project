@extends('layouts.app')

@section('content')
    <form method="post" action="{{route('product.update',$product->id)}}" enctype="multipart/form-data">
        @csrf
        @if(session('status'))
            <div class="alert alert-success col-md-8 offset-md-2" role="alert">
                {{ session('status') }}
            </div>
        @endif
        {{--form inputs--}}
        <div class="row">
            <div class="form-group col-md-8 offset-md-2 ">
                <label for="title">{{__('Title')}}:</label>
                <input type="text" class="form-control title-input" name="title" value="{{$product->title}}" data-validation="required|min:10">
                {!! $errors->first('title', '<p class="error-block">:message</p>') !!}

            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-2  offset-md-2 ">
                <label for="price">{{__('Price')}}:</label>
                <input type="number" class="form-control price-input" name="price" value="{{$product->price}}" data-validation="required|min:10|max:100">{{setting('currency')}}
                {!! $errors->first('price', '<p class="error-block">:message</p>') !!}
            </div>

            <div class="form-group col-md-4">
                <label for="manage-stock">{{__('Manage Stock')}}:</label>
                <select name="manage-stock" class="full-width" style="height: 38px;" id="manage-stock">
                    <option value="1" {{$product->manage_stock?'selected':''}}>{{__('Yes')}}</option>
                    <option value="0" {{$product->manage_stock?'':'selected'}}>{{__('No')}}</option>
                </select>
                {!! $errors->first('manage_stock', '<p class="error-block">:message</p>') !!}
            </div>

            <div class="form-group col-md-2">
                <label for="price">{{__('In-Stock Qty')}}:</label>
                <input type="number" class="form-control price-input" name="stock_availability" value="{{$product->stock_availability}}" data-validation="required|min:1">
                {!! $errors->first('stock_availability', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <label for="thumbnail">{{__('Thumbnail')}}:</label>
                <div class="row file-container">
                    <div class="col-md-3">
                        <img src="{{$product->thumbnail}}"/>
                    </div>
                    <div class="col-md-9">
                        <input type="file" class="form-control file-input" name="thumbnail" value="{{$product->thumbnail}}">
                        <label>{{__('Choose a File')}}</label><br>
                        <span>{{__('or')}}</span><br>
                        <input type="text" name="external-thumb-url" id="external-thumb-url" class="full-width" placeholder=" ex: http://www.birdsandblooms.com/BBam15_RebeccaGranger.jpg">
                        <input type="hidden" name="thumbnail-value" id="thumbnail-value" value="{{$product->thumbnail}}">
                        {!! $errors->first('thumbnail-value', '<p class="error-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <label for="content">{{__('Content')}}:</label>
                <textarea class="form-control content-input" name="content" id="post-content" data-validation="required">{{$product->content}}</textarea>
                {!! $errors->first('content', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <label for="content">{{__('Short Description')}}:</label>
                <textarea class="form-control content-input" name="description" data-validation="required">{{$product->description}}</textarea>

                {!! $errors->first('description', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                <a href="{{route('admin.products')}}" class="btn btn-cancel">{{__('Cancel')}}</a>
            </div>
        </div>
    </form>
@endsection