@extends('layouts.app')

@section('content')
    <div class="container cart-container">
        <div class="row justify-content-center">
            <div class="page-header card-header col-md-12">{{__('My Cart')}}</div>
            @if(cart())
                <form action="{{route('cart.update')}}" method="post">
                    @csrf
                    <div class="col-md-12">
                        @foreach(cart()->items as $cart_item)
                            @include('cart.loop.product')
                        @endforeach
                    </div>
                    <div class="col-md-12 page-footer">
                        {{__('Cart Total: ')}}
                        {{ cartTotalPrice( cart() ) }}
                        {{  setting('currency')}}
                        <a href="{{route('order')}}" class="pull-right btn checkout-btn">{{__('Checkout')}}</a>
                        <button class="pull-right btn update-btn">{{__('Update Cart')}}</button>
                    </div>
                </form>
            @else
                <div class="col-md-12">
                    <h5>{{__('Your cart is currently empty')}}</h5>
                    <a href="{{route('products')}}" style="margin-bottom: 15px" class="btn btn-info">{{__('Continue shopping')}}&rarr;</a>
                </div>
            @endif
        </div>
    </div>
@endsection

