@extends('layouts.app')

@section('content')
    <div class="container cart-container">
        <div class="row ">
            <div class="page-header card-header col-md-12">{{__('Place Order')}}</div>

            <form method="post" action="{{route('order.place')}}" class="col-md-12">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" placeholder="First Name*" class="full-width" data-validation="required">
                            {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input type="text" name="lastname" placeholder="Last Name*" class="full-width" data-validation="required">
                            {!! $errors->first('lastname', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input type="email" name="email"
                                   placeholder="Email Address*"
                                   class="full-width" data-validation="required|email"
                                   value="{{Auth::user()?Auth::user()->email:''}}"
                            >
                            {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="address" placeholder="Address*" class="full-width" data-validation="required">
                            {!! $errors->first('address', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input type="text" name="address2" placeholder="Address Line 2" class="full-width">
                            {!! $errors->first('address2', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <select name="country" class="full-width">
                                @foreach(config('countries') as $country)
                                    <option>{{$country}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="city" placeholder="City*" value="" class="full-width" data-validation="required">
                            {!! $errors->first('city', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input type="text" name="zipcode" placeholder="Zip Code*" class="full-width" data-validation="required">
                            {!! $errors->first('zipcode', '<p class="error-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" placeholder="Phone Number" class="full-width">
                            {!! $errors->first('phone', '<p class="error-block">:message</p>') !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <p>{{__('Payment Method')}}</p>
                    <label><input type="radio" checked name="payment-method" value="cash"> {{__('Cash')}}</label><br>
                    <label><input type="radio" name="payment-method" value="paypal"> {{__('PayPal')}}</label><br>
                    <label><input type="radio" name="payment-method" value="card"> {{__('Bank Card')}}</label>
                </div>

                <div class="col-sm-12">
                    <button class="pull-right btn checkout-btn">{{__('Place Order')}}</button>
                </div>

            </form>

            <div class="col-md-12">
                @foreach(cart()->items as $cart_item)
                    @include('cart.loop.checkoutProduct')
                @endforeach
            </div>
            <div class="col-md-12 page-footer text-right">
                {{__('Cart Total: ')}}
                {{ cartTotalPrice(cart())  }}
                {{setting('currency')}}
            </div>
        </div>
    </div>
@endsection

