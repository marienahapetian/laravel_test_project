@extends('layouts.app')

@section('content')
    <div class="container cart-container">
        <div class="row justify-content-center">
            @if(session('status'))
                <div class="col-md-12 alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="page-header col-md-12">
                {{ __('Order #'.$order->id) }}
            </div>

            <div class="col-md-12 order-info">
                <div class="row">
                    <div class="col-sm-3"><strong>{{__('Transaction #')}}</strong></div>
                    <div class="col-sm-9">{{$order->transaction_id}}</div>
                </div>
            </div>

            <div class="col-md-12 order-info">
                <div class="row">
                    <div class="col-sm-3"><strong>{{__('Total')}}</strong></div>
                    <div class="col-sm-9">{{$order->totalPrice . $order->currency}}</div>
                </div>
            </div>

            <div class="col-md-12 order-info">
                <div class="row">
                    <div class="col-sm-3"><strong>{{__('Status')}}</strong></div>
                    <div class="col-sm-9">{{$order->status->name}}</div>
                </div>
            </div>

            <div class="col-md-12 order-info">
                <div class="row">
                    <div class="col-sm-3"><strong>{{__('Items')}}</strong></div>
                    <div class="col-sm-9">
                        @foreach($order->items as $order_item)
                            <p>
                                <a href="{{route('product.view',$order_item->product->id)}}">
                                    {{$order_item->product_qty.' x '.str_limit($order_item->product->title,50).' - '.$order_item->price*$order_item->product_qty.$order->currency}}
                                </a>
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="page-header col-md-12">
               {{ __('Contact Details') }}
            </div>
            <div class="row col-md-12 order-info">
                @foreach(json_decode($order->order_info) as $key => $order_info)
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <strong>{{ucfirst($key)}}</strong>
                            </div>
                            <div class="col-sm-8">
                                {{$order_info}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

