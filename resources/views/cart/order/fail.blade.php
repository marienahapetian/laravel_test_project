@extends('layouts.app')

@section('content')
    <div class="container cart-container">
        <div class="row justify-content-center">
            <div class="alert alert-danger" role="alert">
                {{ session('status') }}
            </div>
        </div>
    </div>
@endsection

