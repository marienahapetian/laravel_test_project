@extends('layouts.app')

@section('content')
    <div class="container cart-container">
        <div class="row ">
            <h3 class="col-sm-12 page-header">{{__('Order #').$order->id}}</h3>
            <p class="col-sm-12">{{__('Order Placed on '.$order->created_at)}}</p>
        </div>

        <div class="justify-content-center">
            <div class="row table-header">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-4">
                    {{__('Product Name')}}
                </div>
                <div class="col-sm-2">
                    {{__('Product Price')}}
                </div>
                <div class="col-sm-2">
                    {{__('Qty')}}
                </div>
                <div class="col-sm-2">
                    {{__('Total')}}
                </div>
            </div>
            @foreach($order->items as $order_item)
                <div class="row">
                    <div class="col-sm-2">
                        <img src="{{$order_item->product->getThumbnailPhoto()}}"/>
                    </div>
                    <div class="col-sm-4">
                        {{$order_item->product->title}}
                    </div>
                    <div class="col-sm-2">
                        {{ $order_item->price . $order->currency }}
                    </div>
                    <div class="col-sm-2">
                        {{$order_item->product_qty}}
                    </div>
                    <div class="col-sm-2">
                        {{ $order_item->price * $order_item->product_qty . $order->currency }}
                    </div>
                </div>
            @endforeach
        </div>

        <div class="page-footer row text-right">
            <h4 class="col-sm-12">{{ __('Total ') . $order->totalPrice . $order->currency }}</h4>
        </div>
    </div>
@endsection

