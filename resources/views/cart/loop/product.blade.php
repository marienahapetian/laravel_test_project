<div class="row">
    <div class="col-sm-2">
        <img src="{{$cart_item->product->thumbnail}}"/>
    </div>
    <div class="col-sm-4 no-padd-md">
        <h4><a href="{{route('product.view',$cart_item -> product->id)}}">{{str_limit($cart_item->product->title,50)}}</a> </h4>
        <p>{{__('Price: ').$cart_item -> product -> price . setting('currency')}}</p>
        <p>{{str_limit($cart_item -> product -> description,150)}}</p>
    </div>
    <div class="col-sm-2 text-center col-4">
        <input type="number" name="quantity[{{$cart_item->product->id}}]"
               value="{{$cart_item->qty}}"
               min='1'
               max="{{$cart_item->product->manage_stock?$cart_item->product->stock_availability:''}}"/>
        {!! $errors->first("qty-".$cart_item->product->id, '<p class="error-block">:message</p>') !!}
    </div>
    <div class="col-sm-3 no-padd-md col-4">
        {{$cart_item->product->price * $cart_item->qty . setting('currency')}}
    </div>
    <div class="col-sm-1 col-4">
        <a href="{{route( 'product.removeFromCart',$cart_item->product->id )}}"><i class="far fa-minus-square"></i></a>
    </div>
</div>