<div class="row">
    <div class="col-sm-2">
        <img src="{{$cart_item->product->thumbnail}}"/>
    </div>
    <div class="col-sm-4 no-padd">
        <h4><a href="{{route('product.view',$cart_item->product->id)}}">{{ str_limit($cart_item->product->title,50) }}</a> </h4>

        <p>{{str_limit($cart_item->product->description,150)}}</p>
    </div>
    <div class="col-sm-2 text-center">
        <p>{{__('Price: ').$cart_item->product->price . setting('currency')}}</p>
    </div>
    <div class="col-sm-2 text-center">
        <span>{{ $cart_item->qty }}</span>
    </div>
    <div class="col-sm-2 no-padd">
        {{ $cart_item->product->price * $cart_item->qty . setting('currency')}}
    </div>
</div>