@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div>
                            <form method="post" action="" class="search-form">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="title" placeholder="Search by title">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="isbn" placeholder="Search by ISBN">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="author" placeholder="Search by author">
                                </div>
                                <div class="form-group">
                                    <select name="sortby">
                                        <option value="created_at">{{__('Date')}}</option>
                                        <option value="title">{{__('Title')}}</option>
                                        <option value="author">{{__('Author')}}</option>
                                        <option value="isbn">{{__('ISBN')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="asc-desc">
                                        <option value="asc">{{__('Ascending')}}</option>
                                        <option value="desc">{{__('Descending')}}</option>
                                    </select>
                                </div>
                                <button class="btn btn-info">{{__('Search')}}</button>
                            </form>
                            <div id="results-cont"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function() {
            jQuery('.search-form').on('submit', function (e) {
                debugger;
                e.preventDefault();
                var title = jQuery('input[name=title]').val();
                var isbn = jQuery('input[name=isbn]').val();
                var author = jQuery('input[name=author]').val();
                var sortby = jQuery('select[name=sortby]').val();
                var asc = jQuery('select[name=asc-desc]').val();
                jQuery.ajax({
                    type: "GET",
                    url: 'http://book-store.loc/api/book/search',
                    headers: {"Authorization": "Bearer " + "<?php echo setting('library-token');?>"},
                    data: {
                        title: title,
                        isbn: isbn,
                        author: author,
                        sortby: sortby,
                        asc: asc
                    },
                    success: function( response ) {
                        if( response.data && response.data.length ){
                            var results_html = '<table style="width:100%;">';
                            results_html += '<tr>'+
                                '<th>Title</th>'+
                                '<th>ISBN</th>'+
                                '<th>Author</th>'+
                                '<th>Publisher</th>'+
                                '</tr>';

                            jQuery.each(response.data, function(i, item) {
                                results_html += '<tr class="result-item">'+
                                    '<td>'+item.title+'</td>'+
                                    '<td>'+item.isbn+'</td>'+
                                    '<td>'+item.author+'</td>'+
                                    '<td>'+item.publisher+'</td>'+
                                    '</tr>';
                            });
                            results_html += '</table>';
                        } else {
                            var results_html = 'No Search Results';
                        }

                        jQuery('#results-cont').html(results_html);
                    }
                });
            });
        });
    </script>
@endsection