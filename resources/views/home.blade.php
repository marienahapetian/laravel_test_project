@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="section col-md-9">
                            <div class="row"><h2 class="col-md-12"><a href="{{route('products')}}">{{__('Products')}}</a> </h2></div>

                            <div class="row posts">
                                @foreach($products as $product)
                                    @include('products.loop.product')
                                @endforeach
                            </div>
                        </div>

                        <div class="section col-md-3 sidebar">
                            <div class="row"><h2 class="col-sm-12"><a href="{{route('users')}}">{{__('Users')}}</a> </h2></div>

                            <div class="row users">
                                @foreach($users as $user)
                                    @include('users.loop.user',['view'=>'list'])
                                @endforeach
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
