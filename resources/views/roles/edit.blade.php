@extends('admin.layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <form method="post" action="{{route('admin.role.update',$role->id)}}" enctype="multipart/form-data">
        @csrf
        {{--page title--}}
        <div class="clearfix">
            <div class="form-group col-md-8 offset-md-2 page-header">
                {{__('Edit Role')}}
            </div>
        </div>

        {{--form inputs--}}
        <div class="row">
            <div class="form-group col-md-10 offset-md-2 ">
                <label for="title">{{__('Role Name')}}:</label>
                <input type="text" class="form-control title-input" name="name" value="{{$role->name}}" data-validation="required">
            </div>

            <div class="form-group col-md-8 offset-md-2  ">
                <label for="permissions">{{__('Permissions')}}:</label>
                @foreach($permissions as $permission)
                    <div><input type="checkbox" name="permissions[]" {{$role->hasPermissionTo($permission->name)?'checked':''}} value="{{$permission->name}}">{{$permission->name}}</div>
                @endforeach
            </div>
        </div>


        <div class="row">
            <div class="col-md-3"></div>
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-cancel">{{__('Cancel')}}</a>
            </div>
        </div>
    </form>


@endsection
