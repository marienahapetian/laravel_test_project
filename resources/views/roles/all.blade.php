@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <div class="card-header">{{__('User Roles')}}</div>

                    <div class="card-body">
                        @role('super-admin')
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{ route('admin.role.add') }}" class="btn btn-success"><i class="far fa-plus"></i> {{__('Add New')}}</a>
                                </div>
                            </div>
                        @endrole

                        <div class="row">
                            <div class="col-md-1">{{__('#')}}</div>

                            <div class="col-md-2">{{__('Role Name')}}</div>

                            <div class="col-md-2">{{__('Guard Name')}}</div>

                            <div class="col-md-5">{{__('Permissions')}}</div>

                            <div class="col-md-2">{{__('Actions')}}</div>
                        </div>

                        @foreach ($roles as $role)
                            <div class="row">
                                <div class="col-md-1"> {{ $role->id }}</div>

                                <div class="col-md-2"> {{ $role->name }}</div>

                                <div class="col-md-2"> {{ $role->guard_name }}</div>

                                <div class="col-md-5"> {{ $role->permissions->pluck('name')->implode(',') }}</div>

                                <div class="col-md-2">
                                    <a href="{{ route('admin.role.edit', $role->id) }}" class="btn  btn-primary"><i class="fas fa-edit"></i></a>
                                    <a  class="btn  btn-danger" onclick="return confirm('Are you sure you want to delete this post?')" href="{{route('admin.role.delete', $role->id)}}"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        @endforeach
                        {{ $roles->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

