<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{!! asset('img/pen.ico') !!}"/>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    <!-- Scripts -->
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('img/shopify_logo.png')}}"/>
                </a>

                <nav>

                </nav>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('profile') }}">
                                        {{ __('My Profile') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ url('orders') }}">
                                        {{ __('Orders') }}
                                    </a>

                                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('super-admin'))
                                    <a class="dropdown-item" href="{{ url('admin/') }}">
                                        {{ __('Dashboard') }}
                                    </a>
                                    @endif

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ __('Cart') }} <span class="items-count">{{ '('.cartTotalQty( cart() ).')' }}</span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="cartDropdown">
                                <div class="dropdown-item cart-dropdown-item" href="{{ url('cart') }}" style="min-width: 300px">
                                    <div style="padding-left: 0" class="items-cont">
                                        <div class="items">
                                            @if( !cart() || empty(cart()->items) ) {{__('Your Cart is Currently Empty')}}
                                            @else
                                                @foreach(cart()->items as $cart_item)
                                                    <div class="row">
                                                        <div class="col-sm-7 no-padd">
                                                            {{str_limit($cart_item->product->title,20)}}
                                                        </div>
                                                        <div class="col-sm-2">
                                                            {{$cart_item->qty}}
                                                        </div>
                                                        <div class="col-sm-3 no-padd">
                                                            {{$cart_item->product->price . setting('currency')}}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="text-right row cart-total">
                                            @if( cart() && cartTotalQty( cart() ) )
                                                    {!! __('Cart Total:').'<span class="cart-total">'.cartTotalPrice(cart()) . setting('currency').'</span>' !!}
                                            @else
                                                {!! __('Cart Total:').'<span class="cart-total">0' . setting('currency').'</span>' !!}
                                            @endif
                                        </div>
                                        <div class="row">
                                            <a href="{{route('cart')}}" class="view-cart">{{__('View Cart')}}</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <script src="{{ asset('js/app.js') }}" type="text/javascript" rel="script"></script>

    <script src="{{ asset('js/ckeditor.js') }}"></script>

    <script src="{{ asset('js/main.js') }}"></script>


    @yield('scripts')

</body>
</html>
