@extends('admin.layouts.app')

@section('content')
    <form method="post" action="{{route('admin.user.update',$user->id)}}" enctype="multipart/form-data">
        @csrf
        {{--page title--}}
        <div class="clearfix">
            <div class="form-group col-md-10 offset-md-1 page-header">
                {{__('Edit User')}}
            </div>
        </div>

        {{--form inputs--}}
        <div class="row">
            <div class="form-group col-md-10 offset-md-1">
                <label for="name">{{__('Name')}}:</label>
                <input type="text" class="form-control name-input" name="name" value="{{$user->name}}" data-validation="required|min:10">
                {!! $errors->first('name', '<p class="error-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-10 offset-md-1">
                <label for="email">{{__('Email')}}:</label>
                <input type="text" class="form-control email-input" name="email" value="{{$user->email}}" data-validation="required|email">
                {!! $errors->first('email', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-10 offset-md-1">
                <label for="thumbnail">{{__('Thumbnail')}}:</label>
                <div class="row file-container">
                    <div class="col-md-3">
                        <img src="{{$user->thumbnail}}"/>
                    </div>
                    <div class="col-md-9">
                        <input type="file" class="form-control file-input" name="thumbnail" value="{{$user->thumbnail}}">
                        <label>{{__('Choose a File')}}</label><br>
                        <span>{{__('or')}}</span><br>
                        <input type="text" name="external-thumb-url" id="external-thumb-url" class="full-width" placeholder=" ex: http://www.birdsandblooms.com/BBam15_RebeccaGranger.jpg">
                        <input type="hidden" name="thumbnail-value" id="thumbnail-value" value="{{$user->thumbnail}}">
                        {!! $errors->first('thumbnail-value', '<p class="error-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-10 offset-md-1">
                <label for="email">{{__('Role')}}:</label>
                <select name="user-role">
                    @foreach($roles as $role)
                        <option {{$user->hasRole($role->name)?'selected':''}}>
                            {{$role->name}}
                        </option>
                    @endforeach
                </select>
                {!! $errors->first('user-role', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                <a href="{{ url()->previous() }}" class="btn btn-cancel">{{__('Cancel')}}</a>
            </div>
        </div>
    </form>
@endsection
