@extends('admin.layouts.app')

@section('content')

    <div class="card">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        {{--page title--}}
        <div class="clearfix">
           <div class=" page-header">
               {{__('All Users')}}
           </div>
        </div>

        <div class="card-header">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{ route('admin.user.add') }}" class="btn btn-success"><i class="far fa-plus"></i> {{__('Add New')}}</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1"> {{__('User ID')}}</div>
                <div class="col-md-3"> {{__('Name')}}</div>
                <div class="col-md-2"> {{__('Products #')}}</div>
                <div class="col-md-3"> {{__('Created')}}</div>
                <div class="col-md-3"> {{__('Actions')}}</div>
            </div>
        </div>
            <div class="card-body">

                        @foreach ($users as $user)
                            <div class="row">
                                <div class="col-md-1">{{$user->id}}</div>
                                <div class="col-md-3"> {{ $user->name }}<span style="color:#007bff">{{ \Illuminate\Support\Facades\Auth::user()->id==$user->id ? ' (you)' : '' }}</span></div>
                                <div class="col-md-2"> <a href="{{ route('admin.user.products', $user->id) }}">{{ count($user->products) }}</a></div>
                                <div class="col-md-3"> {{$user->created_at}}</div>

                                @can('update-product', $user)
                                    <div class="col-md-3">
                                        <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-primary">{{__('Edit')}}</a>
                                        <a onclick="return confirm('Are you sure you want to delete this user?')" href="{{route('user.delete', $user->id)}}" class="btn btn-danger">
                                            {{__('Delete')}}
                                        </a>
                                    </div>
                                @endcan
                            </div>
                        @endforeach
                        {{ $users->links() }}
                    </div>
    </div>
@endsection
