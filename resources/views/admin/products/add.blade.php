@extends('admin.layouts.app')

@section('content')
    <form method="post" action="{{route('admin.product.create')}}" enctype="multipart/form-data" >
        @csrf

        {{--page title--}}
        <div class="clearfix">
            <div class="form-group col-md-8 offset-md-2 page-header">
                {{__('Add Product')}}
            </div>
        </div>

        {{--form inputs--}}
        <div class="row">
            <div class="form-group col-md-6 offset-md-2 ">
                <label for="title">{{__('Title')}}:</label>
                <input type="text" class="form-control title-input" name="title" data-validation="required|min:10">

                {!! $errors->first('title', '<p class="error-block">:message</p>') !!}
            </div>
            <div class="form-group col-md-2  ">
                <label for="price">{{__('Price')}}:</label>
                <input type="number" class="form-control price-input" name="price" data-validation="required|min:10">{{setting('currency')}}
                {!! $errors->first('price', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <label for="thumbnail">{{__('Thumbnail')}}:</label>
                <div class="row file-container">
                    <div class="col-md-3">
                        <img src="http://pixselo.com/wp-content/uploads/2018/03/dummy-placeholder-image-400x400-300x300.jpg"/>
                    </div>
                    <div class="col-md-9">
                        <input type="file" class="form-control file-input" name="thumbnail" >
                        <label>{{__('Choose a File')}}</label><br>
                        <span>{{__('or')}}</span><br>
                        <input type="text" name="external-thumb-url" id="external-thumb-url" class="full-width" placeholder=" ex: http://www.birdsandblooms.com/BBam15_RebeccaGranger.jpg">
                        <input type="hidden" name="thumbnail-value" id="thumbnail-value" >
                        {!! $errors->first('thumbnail-value', '<p class="error-block">:message</p>') !!}

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <label for="content">{{__('Content')}}:</label>
                <textarea  class="form-control content-input" name="content"  id="post-content" data-validation="required"></textarea>
                {!! $errors->first('content', '<p class="error-block">:message</p>') !!}
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-8 offset-md-2">
                <label for="description">{{__('Short Description')}}:</label>
                <textarea  class="form-control desc-input" name="description" data-validation="required"></textarea>
                {!! $errors->first('description', '<p class="error-block">:message</p>') !!}
            </div>
        </div>


        <div class="row">
            <div class="col-md-3"></div>
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-cancel">{{__('Cancel')}}</a>
            </div>
        </div>
    </form>


@endsection
