@extends('admin.layouts.app')

@section('content')
    <div class="card">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if (session('error'))
           <div class="alert alert-danger" role="alert">
               {{ session('error') }}
           </div>
        @endif

        {{--page title--}}
        <div class="clearfix">
           <div class="page-header">
               {{ucfirst($view) . ' ' . __('Products')}}
           </div>
        </div>

        <div class="col-md-12">
            <a class="tab" href="{{route('admin.products').$related_url}}">{{ $related_text .'('. count($related_products).')'}}</a>
        </div>

        <div class="card-header">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{ route('admin.product.add') }}" class="btn btn-success"><i class="far fa-plus"></i> {{__('Add New')}}</a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1"> {{__('Product ID')}}</div>
                <div class="col-md-3"> {{__('Name')}}</div>
                <div class="col-md-2"> {{__('Price')}}</div>
                <div class="col-md-2"> {{__('Seller')}}</div>
                <div class="col-md-2"> {{__('Created')}}</div>
                <div class="col-md-2"> {{__('Actions')}}</div>
            </div>
        </div>
        <div class="card-body">

            @foreach ($products as $product)
                <div class="row">
                    <div class="col-md-1"> {{ $product->id}}</div>
                    <div class="col-md-3"> {{ $product->title }}</div>
                    <div class="col-md-2"> {{ $product->price . setting('currency') }}</div>
                    <div class="col-md-2"> {{ $product->user->name }}</div>
                    <div class="col-md-2"> {{ $product->created_at}}</div>

                    @can('update-product', $product)
                        <div class="col-md-2">
                            <a href="{{ route('admin.product.edit', $product->id) }}" class="btn btn-primary">{{__('Edit')}}</a>
                            <a onclick="return confirm('Are you sure you want to delete this post?')" href="{{route('product.delete', $product->id)}}" class="btn btn-danger">
                                {{__('Delete')}}
                            </a>
                        </div>
                    @endcan
                </div>
            @endforeach
            {{ $products->links() }}
        </div>
    </div>
@endsection
