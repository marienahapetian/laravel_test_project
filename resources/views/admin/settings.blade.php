@extends('admin.layouts.app')

@section('content')
    <div class="">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif

        <form method="post" action="{{route('admin.update-settings')}}" enctype="multipart/form-data">
                @csrf
                {{--page title--}}
                <div class="clearfix">
                    <div class="form-group col-md-8 offset-md-2 page-header">
                        {{__('Main Settings')}}
                    </div>
                </div>

                @foreach ($settings as $setting)
                    @include('admin.layouts.settings.'.$setting->key, compact('setting'))
                @endforeach

                <div class="row">
                    <div class="form-group col-md-10 text-right">
                        <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                    </div>
                </div>
        </form>

        {{ $settings->links() }}
    </div>
@endsection

