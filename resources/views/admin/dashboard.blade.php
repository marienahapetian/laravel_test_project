@extends('admin.layouts.app')

@section('content')
    Sorry I did not come up with some better class name when I wrote this tutorial in 2017.

    On the desktops, #sidebar.active will mean that the sidebar is hidden and #sidebar only that it's visible.

    It will have an opposite behaviour on mobiles where #sidebar.active is when the sidebar is visible and #sidebar is hidden.
@endsection
