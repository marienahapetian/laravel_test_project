<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{!! asset('img/pen.ico') !!}"/>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/admin/main.css') }}" rel="stylesheet">

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
</head>

<body>
    <div id="app">
        <div class="wrapper">
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <div class="text-right" style="background: cadetblue;margin: -10px -10px 20px;padding: 5px 10px;"><a href="{{route('home')}}" >{{__('Visit Website')}} &rarr;</a></div>
                    <h4>{{__('Admin Dashboard')}}</h4>
                </div>

                <ul class="list-unstyled components">
                    <li class="{{ request()->is('admin/users') ? 'active' : '' }}">
                        <a href="{{route('admin.users')}}" >{{__('Users')}} </a>
                    </li>
                    <li class="{{ request()->is('admin/products') ? 'active' : '' }}">
                        <a href="{{route('admin.products')}}" >{{__('Products')}} </a>
                    </li>
                    <li class="{{ request()->is('admin/orders') ? 'active' : '' }}">
                        <a href="{{route('admin.orders')}}" >{{__('Orders')}} </a>
                    </li>
                    <li class="{{ request()->is('admin/roles') ? 'active' : '' }}">
                        <a href="{{route('admin.roles')}}" >{{__('Roles')}} </a>
                    </li>
                    <li class="{{ request()->is('admin/settings') ? 'active' : '' }}">
                        <a href="{{route('admin.settings')}}" >{{__('Settings')}} </a>
                    </li>
                    <li class="logout-link">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">{{__('Log Out')}} <span style="color:grey">({{\Illuminate\Support\Facades\Auth::user()->name}})</span> </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>

            </nav>

            <!-- Page Content  -->
            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </div>


    {{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>--}}
    <!-- Popper.JS -->
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>--}}
    <!-- Bootstrap JS -->
    {{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" ></script>--}}
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/admin/main.js') }}"></script>
</body>
</html>
