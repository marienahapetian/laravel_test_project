<div class="row">
    <div class="col-md-2 offset-md-2"> {{ ucfirst($setting->key) }}</div>
    <div class="col-md-6">
        <select class="full-width" name="{{$setting->key}}" >
            @foreach(\Illuminate\Support\Facades\Config::get('settings.currencies') as $name=>$symbol)
                <option value="{{$symbol}}" {{$symbol==$setting->value?'selected':''}}>{{$symbol.' ('.$name.')'}}</option>
            @endforeach
        </select>
        <p class="help-text">{{ $setting->help_text }}</p>
    </div>
</div>