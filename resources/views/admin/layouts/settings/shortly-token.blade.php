<div class="row" id="app">
    <div class="col-md-2 offset-md-2"> {{ ucfirst($setting->key) }}</div>
    <div class="col-md-6">
        <textarea name="{{$setting->key}}" style="width: 100%;">{{ $setting->value?:session('shortly_token') }}</textarea>

        <p class="help-text">{{ $setting->help_text }}</p>

        <a href="{{route('shortly.token.get')}}">{{__('Get Token Here')}}</a>
    </div>
</div>