@extends('admin.layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card">
        {{--page title--}}
        <div class="clearfix">
            <div class="page-header">
                {{ucfirst($view) . ' ' . __('Orders') . ' (' . count($orders) . ')'}}
            </div>
        </div>

        <div class="col-md-12 text-right">
            <a class="tab" href="{{route('admin.orders').$related_url}}">{{ $related_text .'('. count($related_orders).')'}}</a>
        </div>

        <div class="card-header">
            <div class="row">
                <div class="col-md-1"> {{__('Order ID')}}</div>
                <div class="col-md-2"> {{__('Order Total')}}</div>
                <div class="col-md-2"> {{__('Status')}}</div>
                <div class="col-md-3"> {{__('User')}}</div>
                <div class="col-md-2"> {{__('Order Placed')}}</div>
                <div class="col-md-2"> {{__('Actions')}}</div>
            </div>
        </div>

        <div class="card-body">
            @foreach ($orders as $order)
                <div class="row {{$order->trashed()?'trashed':''}}">
                    <div class="col-md-1"> {{ $order->id}}</div>
                    <div class="col-md-2"> {{ $order->totalPrice . $order->currency }}</div>
                    <div class="col-md-2"> {{ $order->status->name }}</div>
                    <div class="col-md-3">
                        @if($order->user)
                            <a href="{{route('user.view',$order->user->id)}}">{{ $order->user->name }}</a>
                        @else
                            {{__('guest')}}
                        @endif
                    </div>
                    <div class="col-md-2"> {{ $order->created_at }}</div>

                    <div class="col-md-2">
                        @if($order->trashed())
                            <a onclick="return confirm('Are you sure you want to permanently delete this order?')" href="{{ route('admin.order.delete', $order->id) }}" class="btn btn-primary">{{__('Delete')}}</a>
                            <a onclick="return confirm('Are you sure you want to restore this order?')" href="{{route('admin.order.restore', $order->id)}}" class="btn btn-success">
                                {{__('Restore')}}
                            </a>
                        @else
                            <a href="{{ route('admin.order.edit', $order->id) }}" class="btn btn-primary">{{__('Edit')}}</a>
                            <a onclick="return confirm('Are you sure you want to move this order to trash?')" href="{{route('admin.order.trash', $order->id)}}" class="btn btn-danger">
                                {{__('Trash')}}
                            </a>
                        @endif

                    </div>
                </div>
            @endforeach
            {{ $orders->links() }}
        </div>
    </div>
@endsection
