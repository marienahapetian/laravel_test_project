@extends('admin.layouts.app')

@section('content')
    <form method="post" action="{{route('admin.order.update',$order->id)}}" enctype="multipart/form-data">
        @csrf
        {{--page title--}}
        <div class="clearfix">
            @if(session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-md-10 offset-md-1 page-header">
                    <span class="">{{ __('Order #') . $order->id }}</span>
                    <span class="pull-right">{{ __('Transaction ID : ') . $order->transaction_id }}</span>
                </div>
                <div class="col-md-10 offset-md-1 page-subheader">
                    {{ __('User: ') }} {{ $order->user? $order->user->name : __('guest') }}
                </div>
                <div class="col-md-10 offset-md-1 page-subheader">
                    {{ __('Order Total: ') . $order->totalPrice . $order->currency }}
                </div>
            </div>


            <div class="row">
                {{-- left col--}}
                <div class="justify-content-center col-md-8 offset-md-1  order-items">
                    <div class="row table-header">
                        <div class="col-sm-6 offset-sm-2">
                            {{__('Product')}}
                        </div>
                        <div class="col-sm-1">
                            {{__('Price')}}
                        </div>
                        <div class="col-sm-1">
                            {{__('Qty')}}
                        </div>
                    </div>
                    @foreach($order->items as $order_item)
                        <div class="row {{$order_item->trashed()?'trashed':''}}">
                            <div class="col-sm-2">
                                <img src="{{$order_item->product->getThumbnailPhoto()}}"/>
                            </div>
                            <div class="col-sm-6">
                                {{$order_item->product->title}}
                            </div>
                            <div class="col-sm-1">
                                {{ $order_item->price . $order->currency }}
                            </div>
                            <div class="col-sm-1">
                                {{$order_item->product_qty}}
                            </div>

                            @if($order_item->trashed())
                                <div class="col-sm-2">
                                    <a onclick="return confirm('Are you sure you want to restore this item?')" href="{{route('admin.order.item.restore', $order_item->id)}}" class="btn btn-success">
                                        <i class="fas fa-refresh"></i>
                                    </a>
                                </div>
                            @else
                                <div class="col-sm-2">
                                    <a onclick="return confirm('Are you sure you want to delete this item?')" href="{{route('admin.order.item.delete', $order_item->id)}}" class="btn btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                    @endforeach

                    <div class="row order-info">
                        <div class="page-subheader col-sm-12">{{__('Contact Details')}}</div>
                        @foreach(json_decode($order->order_info) as $key => $order_info)
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <strong>{{ucfirst($key)}}</strong>
                                    </div>
                                    <div class="col-sm-8">
                                        {{$order_info}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>


                {{-- right col--}}
                <div class="justify-content-center col-md-2">
                    <div class="form-group">
                        <label for="order-status">{{__('Order Status')}}</label>
                        <select name="status" id="order-status">
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}" {{$status->id==$order->status_code?'selected':''}}> {{$status->name}} </option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
        </div>

        {{--form inputs--}}

        <div class="row">

        </div>


        <div class="row">
            <div class="col-md-3"></div>
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-success">{{__('Save Changes')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-cancel">{{__('Cancel')}}</a>
            </div>
        </div>
    </form>


@endsection
