<?php

namespace App\Http\Middleware;

use App\Models\Product;
use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use Illuminate\Support\Facades\Auth;


class EditPost extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $product_id = $request->product_id;
        $product = Product::find($product_id);

        if (!Auth::check() && Auth::user()->id != $product->user_id) {
            return redirect('/home');
        }

        return $next($request);
    }
}

