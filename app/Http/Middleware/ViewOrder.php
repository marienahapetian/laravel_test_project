<?php

namespace App\Http\Middleware;

use App\Models\Order;
use Closure;

class ViewOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $order = Order::findOrFail($request->id);
        if($order && auth()->check() && $order->user->id == auth()->user()->id ||  auth()->user()->hasRole('super-admin')) {

            return $next($request);
        }

        return redirect() -> back();
    }
}
