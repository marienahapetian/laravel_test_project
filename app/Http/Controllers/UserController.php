<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the users list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate();
        return view('users.all',['users'=>$users]);
    }

    /**
     * Show the user profile by id.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $user = User::findOrFail($id);
        return view('users.single')->withUser($user);
    }

    /**
     * Show the logged in user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('users.profile')->withUser(Auth::user());
    }

    /**
     * Edit user info.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::findOrFail($id);

        return view('users.edit')->withUser($user);
    }

    /**
     * Edit user info.
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
        ]);

        if($validator->fails()) return redirect()->back() -> withErrors($validator->errors());

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->thumbnail = $request->get('thumbnail-value');
        $user->save();

        return redirect()->back()->with('status', 'Information has been updated');
    }

    /**
     * Show the user posts.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function products($id)
    {
        $user = User::findOrFail($id);

        $canEdit = false;

        if(Auth::user()->id == $id) $canEdit = true;

        return view('users.products',compact('user','canEdit'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::find($id);

        if (Gate::allows('update-user', $user)) {

            $user->products->each->delete();

            $user->delete();

            return redirect()->back()->with('status', 'User has been deleted');

        }

        return redirect('users')->with('error', 'You are not allowed to delete user #'.$id);
    }
}
