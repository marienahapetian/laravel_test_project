<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Response;
use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Intervention\Image\ImageManagerStatic as Image;

class FileUploadController extends Controller
{
    public function upload(Request $request)
    {
        if (Input::hasFile('thumbnail'))
        {
            $image       = $request->file('thumbnail');
            $filename    = $image->getClientOriginalName();

            $orig_image = Image::make($image->getRealPath());

            $orig_image->save(public_path('uploads/' .$filename));

            $orig_image->resize(150, 150);
            $orig_image->save(public_path('uploads/thumb-' .$filename));

            return response()->json([
                'filePath' => url('/uploads/'.$filename),
            ]);
        }
        else{
            return response()->json([
                'error' => 'Could not upload the file',
            ]);
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function fileExists(Request $request)
    {
            $url = $request->post('file');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if($code == 200){
                $status = true;
            }else{
                $status = false;
            }
            curl_close($ch);

        return response()->json([
            'status' => $status,
        ]);
    }
}
