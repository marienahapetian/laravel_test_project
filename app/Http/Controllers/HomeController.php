<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderByRaw('RAND()')->take($this->paginationNumber)->get();
        $users = User::orderByRaw('RAND()')->take($this->paginationNumber)->get();
        return view('home')->withProducts($products)->withUsers($users);
    }
}
