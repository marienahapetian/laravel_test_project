<?php

namespace App\Http\Controllers;

use App\Models\Product;
use \Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Show the posts list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate($this->paginationNumber);
        return view('products.all',['products' => $products]);
    }

    /**
     * Show the post
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $product = Product::findOrFail($id);

        $canEdit = Gate::allows('update-product',$product);

        return view('products.single')->withProduct($product)->withCanEdit($canEdit);
    }

    /**
     * Add New post .
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('products.add');
    }

    /**
     * Edit post info.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        if (Gate::allows('update-product', $product)) {
            return view('products.edit')->withProduct($product);
        }

        return redirect('products')->with('error', 'You are not allowed to edit product #'.$id);

    }

    /**
     * Save new post to db
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'content' => 'required',
            'price' => 'required'
        ]);

        $input = $request->all();

        Product::create([
            'title' => $input['title'],
            'content' => $input['content'],
            'description' => $input['description'],
            'thumbnail' => isset($input['thumbnail-value'])?$input['thumbnail-value']:'',
            'price' => $input['price'],
            'author_id' => Auth::user()->id,
            'manage_stock' => $input['manage_stock'],
            'stock_availability' => $input['stock_availability'],
        ]);

        return redirect('profile')->with('status', 'Information has been updated');
    }

    /**
     * Edit post info.
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'content' => 'required',
            'price' => 'required'
        ]);

        try{
            $product = Product::find($id);
            $product->title = $request->get('title');
            $product->content = $request->get('content');
            $product->description = $request->get('description');
            $product->thumbnail = $request->get('thumbnail-value');
            $product->price = $request->get('price');
            $product->save();
        } catch (\Exception $exception){
            return redirect()->back()->with('error',$exception->getMessage());
        }

        return redirect( )->back()->with('status', 'Information has been updated');
    }

    /**
     * Delete Post
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Product::findOrFail($id);

        if (Gate::allows('update-product', $product)) {

            $product->delete();

            return redirect()->back()->with('status', 'Post has been deleted');

        }

        return redirect('products')->with('error', 'You are not allowed to delete product #'.$id);
    }

}
