<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index()
    {
        return view('cart.index');
    }

    /**
     * @param Request $request
     * @param $product_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToCart( Request $request, $product_id )
    {
        try{
            $product = Product::findOrFail($product_id);
            $quantity = $request->get('quantity')?:1;

            if( Auth::check() ){
                $cart = Cart::firstOrCreate( ['user_id' => Auth::user()->id] );

                $cart = $this->addToCartDB( $cart, $product,$quantity);

                if($cart) return response()->json(
                    [
                        'success' => 1,
                        'total' => static::getCartTotalPrice($cart),
                        'items_number' => static::getCartTotalQty($cart),
                        'currency' => setting('currency'),
                        'items' => $cart->items,
                    ]
                );
                else return response()->json(
                    [
                        'success' => 0,
                        'message' => 'Only '.$product->stock_availability.' items in store',
                    ]
                );


            } else {
                $cart = Session::get('cart')?:null;

                $cart =  $this -> addToCartSession( $cart, $product, $quantity );

                if($cart) {
                    $cart = (object) $cart;

                    $request -> session() -> put('cart',$cart);

                    return response()->json(
                        [
                            'success' => 1,
                            'total' => static::getCartTotalPrice($cart),
                            'items_number' => static::getCartTotalQty($cart),
                            'currency' => setting('currency'),
                            'items' => $cart->items,
                        ]
                    );
                } else {
                    return response()->json(
                        [
                            'success' => 0,
                            'message' => 'Only '.$product->stock_availability.' items in store',
                        ]
                    );
                }
            }
        } catch (Exception $exception){
            return response()->json(
                [
                    'success' => 0,
                    'message' => $exception->getMessage(),
                ]
            );
        }


    }


    /**
     * Remove the product from the cart
     * @param Request $request
     * @param $product_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeFromCart(Request $request,$product_id)
    {
        $product = Product::findorFail($product_id);

        if(Auth::check()) {
            $cart = Cart::where( 'user_id',Auth::user()->id )->first();
            $cart = $this -> removeFromDB( $cart, $product );
        } else {
            $cart = $request->session()->get('cart');
            $cart = $this -> removeFromSession( $cart,$product );
            $request -> session() -> put('cart',$cart);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update( Request $request )
    {
        $quantity = $request->get('quantity');

        if(Auth::check()) {
            $cart = Cart::where('user_id','=',Auth::user()->id)->first();

            $this->updateCartDB( $cart, $request );
        } else {
            $cart = $request->session()->get('cart');

            $cart = $this->updateCartSession( $cart, $request, $quantity );

            $request->session()->put('cart',$cart);
        }

        return redirect() -> back();
    }

    /**
     * @param $cart
     * @param Product $product
     * @param int $quantity
     * @return array
     */
    public function addToCartSession( $cart, Product $product, $quantity = 1)
    {
        $totalPrice = 0;
        $totalQty = 0;
        $cart = (array) $cart;

        /* item does not exist in the cart */
        if(empty($cart) || !count($cart['items']) || !isset($cart['items'][$product->id])) {
            if($product->manage_stock && $product->stock_availability <  $quantity) return false;

            $cart['items'][$product->id] = (object)['qty' => $quantity,'product' => $product];
        }
        /* item already in the cart */
        else if(isset($cart['items'][$product->id])){
            if($product->manage_stock && $product->stock_availability <  $cart['items'][$product->id]->qty + $quantity) return false;

            $item =  $cart['items'][$product->id] ;
            $item->qty += $quantity;
            $cart['items'][$product->id] = (object) $item;
        }


        foreach ($cart['items'] as $item){
            $totalPrice += $item->qty * $item->product->price;
            $totalQty += $item->qty ;
        }

        $cart['totalPrice'] = $totalPrice;
        $cart['totalQty'] = $totalQty;

        return $cart;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return Cart | false
     */
    public function addToCartDB( Cart $cart, Product $product,$quantity = 1)
    {
        $items_ids = array_pluck($cart->items,'product_id');

        if(in_array($product->id,$items_ids)) {

            foreach ($cart->items as $key=>$item){
                if($item->product_id == $product->id){
                    if($product->manage_stock && $product->stock_availability < ($item->qty + $quantity)) return false;

                    $item->qty += $quantity;
                    $item->save();
                }
            }

        } else {

            if( $product->manage_stock && $product->stock_availability < $quantity ) return false;

            CartItem::create([
                'cart_id' => $cart->id,
                'product_id' => $product->id,
                'qty' => $quantity
            ]);

            $cart->items = CartItem::where('cart_id',$cart->id)->get();

        }

        return $cart;
    }

    /**
     * @param $cart
     * @param Product $product
     * @return mixed
     */
    public function removeFromSession($cart, Product $product)
    {
        if($cart->items){
            if(key_exists($product->id,$cart->items)) {
                $cart_item = $cart->items[$product->id];
                $cart->totalQty -= $cart_item->qty;
                $cart->totalPrice -= $cart_item->qty * $product->price;
                unset($cart->items[$product->id]);
            }
        }

        return $cart;
    }

    /**
     * @param $cart
     * @param Product $product
     * @return mixed
     */
    public function removeFromDB($cart, Product $product)
    {
        if($cart->items){
            $items_ids = array_pluck($cart->items,'product_id');

            if(in_array($product->id,$items_ids)) {
                $item = CartItem::where([
                    ['product_id', $product->id],
                    ['cart_id', $cart->id]
                ])
                    ->first();

                $cart->save();
                $item->delete();
            }
        }

        return $cart;
    }

    /**
     * @return mixed
     */
    public static function getCart()
    {
        if(Auth::check()) return Cart::where('user_id', '=', Auth::user()->id)->first();
        return $cart = \Illuminate\Support\Facades\Session::get('cart');
    }

    public static function getCartTotalQty( $cart )
    {
        $qty = 0;
        if( $cart && $cart->items ){
            foreach ($cart->items as $item){
                $qty += $item->qty;
            }
        }

        return $qty;
    }

    public static function getCartTotalPrice( $cart )
    {
        $price = 0;
        if( $cart && $cart->items ){
            foreach ($cart->items as $item){
                $price += $item->qty * $item->product->price;
            }
        }

        return $price;
    }


    /**
     * @param Cart $cart
     * @param Request $request
     * @param $quantity
     * @return Cart
     */
    public function updateCartSession( Cart $cart, Request $request, $quantity )
    {
        $items = $cart->items;
        $totalQty = 0;
        $totalPrice = 0;
        foreach ($cart->items as $key => $item){
            if( key_exists($item->product->id,$quantity) ){

                if(!$item->product->manage_stock || $item->product->stock_availability >= $quantity){
                    $item->qty = $quantity[$item->product->id];
                }

                $items[$key] = $item;

                $totalQty += $item->qty;
                $totalPrice += $item->qty * $item->product->price ;
            }
        }

        $cart->items = $items;
        $cart->totalPrice = $totalPrice;
        $cart->totalQty = $totalQty;

        return $cart;
    }

    /**
     * @param Cart $cart
     * @param Request $request
     * @return Cart|bool
     */
    public function updateCartDB( Cart $cart, Request $request )
    {
        if(!$cart->items) return false;

        $quantity = $request->get('quantity');

        foreach ($quantity as $prod_id => $qty){
            $item = CartItem::where([
                ['product_id',$prod_id],
                ['cart_id', $cart->id]
            ]) -> first();

            if(!$item->product->manage_stock || $item->product->stock_availability >= $qty )
                $item->qty = $qty;

            $item->save();
        }

        $cart -> save();

        return $cart;
    }
}
