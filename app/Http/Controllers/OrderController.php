<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkout()
    {
        return view('cart.checkout');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id)
    {
        $order = Order::findOrFail($id);
        $this->updateTotalPrice( $order );

        return view('cart.order.single',compact('order'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orders()
    {
        $orders = Order::where('user_id',Auth::user()->id)->paginate();

        foreach ($orders as $key=>$order){
            $orders[$key] = $this->updateTotalPrice($order);
        }

        return view('users.orders',compact('orders'));
    }
    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success($id)
    {
        $order = Order::findOrFail($id);
        $this->updateTotalPrice( $order );

        return view('cart.order.success')->with(compact('order'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fail()
    {
        return view('cart.order.fail');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function placeOrder(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'lastname'=>'required',
            'address'=>'required',
            'email'=>'required|email',
            'city'=>'required',
            'zipcode'=>'required',
        ]);

        try{
            if( cart()->items ){
                $order_info = json_encode( $request->except(['_token','payment_method']) );

                $order = Order::create([
                    'user_id' => Auth::check() ? Auth::user()->id : 0,
                    'payment_method' => $request->get('payment-method'),
                    'status_code' => 1,
                    'currency' => setting('currency'),
                    'transaction_id'=> $this->generateTransactionId(),
                    'order_info' => $order_info
                ]);

                foreach ( cart()->items as $cart_item ){
                        /* place order */
                        OrderItem::create([
                            'product_id' => $cart_item->product->id,
                            'order_id' => $order->id,
                            'product_qty' => $cart_item->qty,
                            'price' => $cart_item->product->price
                        ]);

                        /* decrease product qty in stock */
                        $product = Product::find($cart_item->product->id);
                        if($product->manage_stock) $product->stock_availability -= $cart_item->qty;
                        $product->save();
                }
            }

            if(Auth::check()){
                $cart = Cart::where('user_id',Auth::user()->id)->first();
                foreach ($cart->items as $item){
                    $item->delete();
                }
                $cart->delete();
            } else {
                $request->session()->forget('cart');
            }

            return redirect()->route('order.success', [$order])->with('status','Order placed successfully!');
        } catch (\Exception $exception){
            return redirect('order/fail')->with('status',$exception->getMessage());
        }

    }

    private function generateTransactionId()
    {
        do {
            $trans_id = Str::random();
        } while ($this->transIdExists($trans_id));

        return $trans_id;
    }

    private function updateTotalPrice( Order $order)
    {
        foreach($order->items as $item){
            $order->totalPrice += $item->product_qty * $item->product->price;
        }

        return $order;
    }

    private function transIdExists($string)
    {
        return Order::where('transaction_id',$string) -> exists();
    }
}
