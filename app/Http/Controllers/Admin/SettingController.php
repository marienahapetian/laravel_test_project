<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class SettingController extends Controller
{
    /**
 * display settings page
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
    public function index()
    {
        $settings = Setting::paginate($this->paginationNumber);
        return view('admin.settings',compact('settings'));
    }

    /**
     * update settings
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request)
    {
        $settings = $request->except('_token');

        foreach ($settings as $key=>$value){
            Setting::where('key', $key)->update(['value' => $value]);
        }

        return Redirect::back()->with('status','Changes Saved Successfully !');
    }
}
