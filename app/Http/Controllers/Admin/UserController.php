<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::paginate();
        return view('admin.users.all',compact('users'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::all();
        return view('admin.users.edit',compact('user','roles'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $roles = Role::all();
        return view('admin.users.add',compact('roles'));
    }

    /**
     * Save new post to db
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input = $request->all();

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'password'=>'required|min:6',
            'user-role' => 'required',
        ]);

        try{
            $user = User::create([
                'name'=>$input['name'],
                'email'=>$input['email'],
                'thumbnail'=>isset($input['thumbnail-value'])?$input['thumbnail-value']:'',
                'password'=>bcrypt($input['password'])
            ]);
        } catch (QueryException $exception){
            return back()->withError($exception->errorInfo[2])->withInput();
        }



        $user->syncRoles($request->get('user-role'));

        return redirect('admin/users')->with('status', 'User has been added');
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,$id)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'user-role' => 'required',
        ]);

        if($validator->fails()) return redirect()->back() -> withErrors($validator->errors());

        try{
            $user = User::find($id);
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->thumbnail = $request->get('thumbnail-value');
            $user->syncRoles($request->get('user-role'));
            $user->save();
        } catch (\Exception $exception){
            return redirect('admin/users')->with('error', $exception->getMessage());
        }

        if(Auth::user()->hasRole('super-admin')) return redirect('admin/users')->with('status', 'Information has been updated');

        return redirect('admin/users')->with('status', 'Information has been updated');
    }

    /**
     * Show the user posts.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function products($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.products',compact('user'));
    }
}
