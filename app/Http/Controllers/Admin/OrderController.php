<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * param string $view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index( $view = 'all')
    {
        if($view == 'trashed'){
            $related_text = 'All';
            $related_url = '';
            $related_orders = Order::paginate();
            $orders = Order::onlyTrashed()->paginate();
        } else {
            $related_text = 'Trashed';
            $related_url = '/trashed';
            $orders = Order::paginate($this->paginationNumber);
            $related_orders = Order::onlyTrashed()->paginate();
        }

        foreach ($orders as $order){
            $this->updateTotalPrice( $order );
        }

        return view('admin.orders.all',compact('view','related_orders','orders','related_url','related_text'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);

        $this->updateTotalPrice( $order );

        $statuses = OrderStatus::all();

        return view('admin.orders.edit',compact('order','statuses'));

    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request,$id)
    {
        $order = Order::findOrFail($id);
        $order -> status_code = $request->get('status');
        $order -> save();

        return redirect()->back()->with('status','Order Saved Successfully!');
    }

    private static function updateTotalPrice( Order $order)
    {
        foreach($order->items as $item){
            $order->totalPrice += $item->product_qty * $item->product->price;
        }

        return $order;
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        Order::withTrashed()->where('id', $id)->first()->restore();

        return redirect('admin/orders')->with('status','Order restored successfully!');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function trash($id)
    {
        $order = Order::findOrFail($id);

        $order->delete();

        return redirect('admin/orders')->with('status','Order moved to trash!');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $order = Order::withTrashed()->where('id', $id)->first();

        $order->forceDelete();

        return redirect('admin/orders')->with('status','Order deleted successfully!');
    }
}
