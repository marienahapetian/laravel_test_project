<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Show the posts list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate($this->paginationNumber);
        return view('roles.all',compact('roles'));
    }

    /**
     * Add New post .
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $permissions = Permission::all();
        return view('roles.add',compact('permissions'));
    }

    /**
     * Edit post info.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = Permission::all();

        $role = Role::findOrFail($id);

        return view('roles.edit',['role'=>$role,'permissions'=>$permissions]);

    }

    /**
     * Save new post to db
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input = $request->all();

        $request->validate([
            'name' => 'required',
        ]);

        try {
            $role = Role::create(['guard_name' => 'web', 'name' => $input['name']]);
            $role->givePermissionTo($input['permissions']);
            return redirect('admin/roles')->with('status', 'Role has been added successfully');
        } catch (\Exception $exception) {
            return redirect('admin/roles')->with('error', $exception->getMessage());
        }


    }

     /**
     * Edit role info.
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if($validator->fails()) return redirect()->back() -> withErrors($validator->errors());

        try{
            $role = Role::findById($id);

            $role->syncPermissions($request->input('permissions'));

            $role->name = $request->input('name');

            $role->save();
        } catch (\Exception $exception){
            return redirect('admin/roles')->with('error', $exception->getMessage());
        }


        return redirect()->back()->with('status', 'Role saved successfully');

    }

    /**
     * Delete Role
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $role = Role::findOrFail($id);

        if (Auth::user()->hasRole('super-admin')) {

            $role->delete();

            return redirect()->back()->with('status', 'Role has been deleted');

        }

        return redirect('admin.roles')->with('error', 'You are not allowed to delete role #'.$id);
    }

}
