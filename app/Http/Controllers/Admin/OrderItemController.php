<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderItemController extends Controller
{

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        OrderItem::withTrashed()->where('id', $id)->restore();

        $order_item = OrderItem::findOrFail($id);

        $order_item->restore();

        return redirect()->back()->with('status','Item restored successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $order_item = OrderItem::findOrFail($id);

        $order_item->delete();

        return redirect()->back()->with('status','Item removed from order');
    }
}
