<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class ProductController extends Controller
{

    /**
     * Show the posts list.
     * @param string $view
     * @return \Illuminate\Http\Response
     */
    public function index( $view = 'all' )
    {
        if($view == 'trashed'){
            $related_text = 'All';
            $related_url = '';
            $products = Product::onlyTrashed()->paginate($this->paginationNumber);
            $related_products = Product::orderBy('created_at', 'desc')->paginate($this->paginationNumber);
        } else {
            $related_text = 'Trashed';
            $related_url = '/trashed';
            $products = Product::orderBy('created_at', 'desc')->paginate($this->paginationNumber);
            $related_products = Product::onlyTrashed()->paginate($this->paginationNumber);

        }
        return view('admin.products.all',compact('view','related_text','related_url','related_products','products'));
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('admin.products.edit',compact('product'));
    }

    public function add()
    {
        return view('admin.products.add');
    }

    /**
     * Save new post to db
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $input = $request->all();

        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'content' => 'required',
            'price' => 'required'
        ]);

        Product::create([
            'title'=>$input['title'],
            'content'=>$input['content'],
            'description'=>$input['description'],
            'thumbnail'=>isset($input['thumbnail-value'])?$input['thumbnail-value']:'',
            'price'=>$input['price'],
            'author_id'=>Auth::user()->id
        ]);

        return redirect('admin/products')->with('status', 'Product has been added');
    }

    /**
     * Update product
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'content' => 'required',
            'price' => 'required'
        ]);

        if($validator->fails()) return redirect()->back() -> withErrors($validator->errors());

        $product = Product::find($id);
        $product->title = $request->get('title');
        $product->content = $request->get('content');
        $product->description = $request->get('description');
        $product->thumbnail = $request->get('thumbnail-value');
        $product->price = $request->get('price');
        $product->stock_availability = $request->get('stock_availability');
        $product->manage_stock = $request->get('manage_stock');
        $product->save();

        if(Auth::user()->hasRole('super-admin')) return redirect('admin/products')->with('status', 'Information has been updated');
        return redirect()->back()->with('status', 'Information has been updated');
    }
}
