<?php

function setting($key)
{
    $settings = array_pluck(app('settings'),'value','key');

    return array_get($settings, $key);
}

function cart()
{
    return \App\Http\Controllers\CartController::getCart();
}

function cartTotalPrice( $cart )
{
    return \App\Http\Controllers\CartController::getCartTotalPrice($cart);
}

function cartTotalQty( $cart )
{
    return \App\Http\Controllers\CartController::getCartTotalQty($cart);
}