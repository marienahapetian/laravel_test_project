<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class Cart extends Model
{
    public $primarykey = 'user_id';

    public $table = 'cart';

    protected $fillable = ['user_id'];

    protected $totalPrice = 0;

    protected $totalQty = 0;

    public function user(){
        return $this->hasOne('App\User');
    }

    public function items()
    {
        return $this->hasMany('App\Models\CartItem','cart_id');
    }
}
