<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','payment_method','status_code','transaction_id','currency','order_info'
    ];

    protected $perPage = 10;  //default 25

    public $totalPrice = 0;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public static function boot() {
        parent::boot();

        static::deleting( function($order) {
            // delete items when deleting order
            if ($order->forceDeleting) {
                $order->items()->withTrashed()->get()
                    ->each(function($item) {
                        $item->forceDelete();
                    });
                $order->items()->withTrashed()->forceDelete();
            }
            // trash items when trashing order
            else {
                $order->items()->get()
                    ->each(function($item) {
                        $item->delete();
                    });
                $order->items()->delete();
            }
        });

        // restore items when restoring order
        static::restoring( function($order) {
            $order->items()->withTrashed()->get()
                ->each(function($item) {
                    $item->restore();
                });
            $order->items()->withTrashed()->restore();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne('App\Models\OrderStatus','id', 'status_code');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Models\OrderItem','order_id','id')->withTrashed();
    }

}
