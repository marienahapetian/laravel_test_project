<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\Facades\Image;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','content','author_id','thumbnail','description','price',
        'manage_stock','stock_availability'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getThumbnailPhoto()
    {
        if(strpos($this->attributes['thumbnail'],url('uploads'))!==false){
            $filename = 'thumb-'.str_replace(url('uploads').'/',"",$this->attributes['thumbnail']);
            return url('uploads').'/'.$filename;
        }

        return $this->getThumbnailAttribute();
    }

    public function getThumbnailAttribute()
    {
        return $this->attributes['thumbnail']?:'http://pixselo.com/wp-content/uploads/2018/03/dummy-placeholder-image-400x400-300x300.jpg';
    }

    /**
     * Get the author of the post.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function order_item()
    {
        return $this->belongsToMany('App\Models\OrderItem');
    }

    public function cart_item()
    {
        return $this->belongsToMany('App\Models\CartItem');
    }

}
