<?php

namespace App\Providers;

use App\Models\Order;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-product', function (User $user, $product) {
            return $user->hasRole('super-admin') || $user->id == $product->author_id;
        });

        Gate::define('update-user', function (User $user1, User $user2) {
            return $user1->hasRole('super-admin') || $user1->id == $user2->id;
        });

        Gate::define('update-order', function (User $user, Order $order) {
            return $user->hasRole('super-admin');
        });

    }
}
