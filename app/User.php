<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;


class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','thumbnail'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $perPage = 10;  //default 25


    /**
     * @return string thumbnail url
     */
    public function getThumbnailPhoto()
    {
        if(strpos($this->attributes['thumbnail'],url('uploads'))!==false){
            $filename = 'thumb-'.str_replace(url('uploads').'/',"",$this->attributes['thumbnail']);
            return url('uploads').'/'.$filename;
        }

        return $this->getThumbnailAttribute();
    }

    /**
     * @return string avatar url
     */
    public function getThumbnailAttribute()
    {
        return $this->attributes['thumbnail']?:'http://pixselo.com/wp-content/uploads/2018/03/dummy-placeholder-image-400x400-300x300.jpg';
    }

    /**
     * Get the posts of the user.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'author_id');
    }

    /**
     * Get orders of the user
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'user_id');
    }


}
