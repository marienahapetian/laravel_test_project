<?php

use \Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/welcome', function (){
    return view('welcome');
});

Route::get('/', function (){
    return redirect('/home');
});

Route::get('/profile', 'UserController@profile');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/user/delete/{id}', 'UserController@delete')->where('id', '[0-9]+')->name('user.delete');
Route::get('/user/{id}/edit', 'UserController@edit')->where('id', '[0-9]+')->name('user.edit');
Route::get('/user/{id}/products', 'UserController@products')->where('id', '[0-9]+')->name('user.products');
Route::get('/user/{id}', 'UserController@view')->where('id', '[0-9]+')->name('user.view');

Route::post('/user/update/{id}','UserController@update')->where('id', '[0-9]+')->name('user.update');

Route::get('/orders', 'OrderController@orders');
Route::get('/order/view/{id}', 'OrderController@view')->where('id', '[0-9]+')->middleware('\App\Http\Middleware\ViewOrder::class')->name('order.view');


Route::get('/products', 'ProductController@index')->name('products');
Route::get('/product/add', 'ProductController@add')->name('product.add');
Route::get('/product/delete/{id}', 'ProductController@delete')->where('id', '[0-9]+')->name('product.delete');
Route::get('/product/{id}/edit', 'ProductController@edit')->where('id', '[0-9]+')->name('product.edit');
Route::get('/product/{id}', 'ProductController@view')->where('id', '[0-9]+')->name('product.view');

Route::post('/product/add-to-cart/{id}', 'CartController@addToCart')->where('id', '[0-9]+')->name('product.addToCart');

Route::get('/product/add-to-cart/{id}', 'CartController@addToCart')->where('id', '[0-9]+')->name('product.addToCart');
Route::get('/product/remove-from-cart/{id}', 'CartController@removeFromCart')->where('id', '[0-9]+')->name('product.removeFromCart');
Route::get('/product/reduce-cart-qty/{id}', 'CartController@reduceCartQty')->where('id', '[0-9]+')->name('product.reduceCartQty');

Route::post('/product/create', 'ProductController@create')->name('product.create');
Route::post('/product/update/{id}','ProductController@update')->where('id', '[0-9]+')->name('product.update');

Route::post('/cart/update', 'CartController@update')->name('cart.update');

Route::get('/send-mail', 'EmailController@send');

Route::group( [ 'namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => '\App\Http\Middleware\IsAdmin::class'], function()
{
    Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@index']);
    Route::get('/users', 'UserController@index')->name('admin.users');
    Route::get('/user/add', 'UserController@add')->name('admin.user.add');
    Route::post('/user/create', 'UserController@create')->name('admin.user.create');
    Route::post('/user/update/{id}', 'UserController@update')->where('id', '[0-9]+')->name('admin.user.update');
    Route::get('/user/{id}', 'UserController@edit')->where('id', '[0-9]+')->name('admin.user.edit');
    Route::get('/user/{id}/products', 'UserController@products')->where('id', '[0-9]+')->name('admin.user.products');


    Route::get('/products/{name?}', 'ProductController@index')->where(['name' => 'all|trashed'])->name('admin.products');
    Route::get('/product/add', 'ProductController@add')->name('admin.product.add');
    Route::post('/product/create', 'ProductController@create')->name('admin.product.create');
    Route::post('/product/update/{id}', 'ProductController@update')->where('id', '[0-9]+')->name('admin.product.update');
    Route::get('/product/{id}', 'ProductController@edit')->where('id', '[0-9]+')->name('admin.product.edit');

    Route::get('/orders/{name?}', 'OrderController@index')->where(['name' => 'all|trashed'])->name('admin.orders');
    Route::get('/order/edit/{id}', 'OrderController@edit')->where('id', '[0-9]+')->name('admin.order.edit');
    Route::post('/order/update/{id}', 'OrderController@update')->where('id', '[0-9]+')->name('admin.order.update');
    Route::get('/order/delete/{id}', 'OrderController@delete')->where('id', '[0-9]+')->name('admin.order.delete');
    Route::get('/order/trash/{id}', 'OrderController@trash')->where('id', '[0-9]+')->name('admin.order.trash');
    Route::get('/order/restore/{id}', 'OrderController@restore')->where('id', '[0-9]+')->name('admin.order.restore');
    Route::get('/order/item/delete/{id}', 'OrderItemController@delete')->where('id', '[0-9]+')->name('admin.order.item.delete');
    Route::get('/order/item/restore/{id}', 'OrderItemController@restore')->where('id', '[0-9]+')->name('admin.order.item.restore');

    Route::get('/settings', 'SettingController@index')->name('admin.settings');
    Route::post('/update-settings', 'SettingController@update')->name('admin.update-settings');

    Route::get('/roles', 'RoleController@index')->name('admin.roles');
    Route::get('/role/add', 'RoleController@add')->name('admin.role.add');
    Route::get('/role/edit/{id}', 'RoleController@edit')->where('id', '[0-9]+')->name('admin.role.edit');
    Route::post('/role/update/{id}', 'RoleController@update')->where('id', '[0-9]+')->name('admin.role.update');
    Route::post('/role/create', 'RoleController@create')->name('admin.role.create');
    Route::get('/role/delete/{id}', 'RoleController@delete')->where('id', '[0-9]+')->name('admin.role.delete');


});

Route::get('/cart','CartController@index')->name('cart');
Route::get('/order','OrderController@checkout')->name('order');
Route::get('/order/success/{id}','OrderController@success')->name('order.success');
Route::get('/order/fail','OrderController@fail')->name('order.fail');
Route::post('/place-order','OrderController@placeOrder')->name('order.place');


Route::post('/upload-file','FileUploadController@upload')->name('upload');
Route::post('/check-file-exists','FileUploadController@fileExists');

Route::get('/redirect',function (){
    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => 'http://laravel-blog.loc/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://shortly.web/oauth/authorize?'.$query);
})->name('shortly.token.get');


Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://shortly.web/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '3',
            'client_secret' => 'eSVs4xy1xoGxoQMvCn6klmN95AKh627tzFyq44pZ',
            'redirect_uri' => 'http://laravel-blog.loc/callback',
            'code' => $request->code,
        ],
    ]);


    $response = json_decode((string) $response->getBody(), true);

    return redirect()->back()->with('shortly_token',$response['access_token']);
});

Route::get('/books', function (){
    return view('book-api');
});

Route::get('/shortlinks', function (){
    return view('shortlinks-api');
});


Route::get('/clear-cache', function() {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
});



