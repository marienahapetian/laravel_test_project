$(function() {
    function ValidateForm(e) {
        var inputs = $('form').find('input[data-validation],textarea[data-validation]');
        var validation_errors = [];
        var error_blocks = $('.error-block');

        error_blocks.remove();

        inputs.each(function () {
            var input = $(this);
            var validation_rules = $(this).attr('data-validation').split('|');
            $.each( validation_rules, function( i, validation_rule ) {
                if(validate(input,validation_rule)!==true) validation_errors.push(validate(input,validation_rule));
            });
        })

        if(validation_errors.length){
            e.preventDefault();
            $.each( validation_errors, function( i, validation_error ) {
                $(validation_error.input+'[name="'+validation_error.name+'"]').closest('.form-group').append('<p class="error-block">'+validation_error.text+'</p>');
            });

            $([document.documentElement, document.body]).animate({
                scrollTop: $("p.error-block").eq(0).prev().offset().top - 15
            }, 1000);
            return false;
        }

        return true;
    }

    function ValidateEmail(text) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(text)) return true;
        return false;

    }

    function validate(input,rule) {
        var nodename = input.attr('name').charAt(0).toUpperCase() + input.attr('name').slice(1);
        if(rule=='required'){
            if(input.val()=='') return {
                input:  input[0].nodeName,
                name: input.attr('name'),
                text:  nodename +' cannot be blank',
            };
        } else if(rule=='email'){
            if(!ValidateEmail(input.val())) return {
                input:  input[0].nodeName,
                name: input.attr('name'),
                text:  'Invalid value for '+input.attr('name')+' field',
            };
        } else if(rule.match(/^min:([0-9]+)$/)){
            var limit = rule.replace("min:", "");
            if(input[0].nodeName == 'INPUT' && input[0].type == 'number' && parseFloat(input.val()) < parseFloat(limit)) {
                return {
                    input:  input[0].nodeName,
                    name: input.attr('name'),
                    text: nodename +' cannot be smaller than '+limit,
                }
            } else if((input[0].nodeName == 'INPUT' && input[0].type !== 'number' && input.val().length < limit) || (input[0].nodeName == 'TEXTAREA'  && input.val().length < limit) ){
                return {
                    input:  input[0].nodeName,
                    name: input.attr('name'),
                    text: nodename +' cannot be shorter than '+limit+ ' chars',
                }
            }

        } else if(rule.match(/^max:([0-9]+)$/)){
            var limit = rule.replace("max:", "");
            if(input[0].nodeName == 'INPUT' && input[0].type == 'number' && parseFloat(input.val()) > parseFloat(limit)) {
                return {
                    input:  input[0].nodeName,
                    name: input.attr('name'),
                    text: nodename +' cannot be bigger than '+limit,
                }
            } else if((input[0].nodeName == 'INPUT' && input[0].type !== 'number' && input.val().length > limit) || (input[0].nodeName == 'TEXTAREA'  && input.val().length > limit) ) {
                return {
                    input:  input[0].nodeName,
                    name: input.attr('name'),
                    text: nodename +' cannot be longer than '+limit+ ' chars',
                }
            }
        }

        return true;
    }

    function ValidURL(str) {
        pattern =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if(!pattern.test(str)) {
            alert("Please enter a valid URL.");
            return false;
        } else {
            return true;
        }
    }

    function checkImg(url) {
        return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
    }



    $('.file-container #external-thumb-url').on('blur',function () {
        var _this = $(this);
        var url = $(this).val();

        if(ValidURL(url) && checkImg(url)) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: '/check-file-exists',
                type:'HEAD',
                file: url,
                dataType: 'json',
                contentType: false,
                error: function()
                {
                    //file not exists
                },
                success: function()
                {
                    //file exists
                    _this.closest('.file-container').find('img').attr('src',url);
                    _this.closest('.file-container').find('input#thumbnail-value').val(url);
                }
            });

        }
    })

    $('.file-container img,.file-container label').on('click',function () {
        $(this).closest('.file-container').find('input[type=file]').trigger('click');
    });

    $('.file-container #external-thumb-url').on('blur',function () {
        var _this = $(this);
        var url = $(this).val();

        if(ValidURL(url) && checkImg(url)) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: '/check-file-exists',
                type:'HEAD',
                file: url,
                dataType: 'json',
                contentType: false,
                error: function()
                {
                    //file not exists
                },
                success: function()
                {
                    //file exists
                    _this.closest('.file-container').find('img').attr('src',url);
                    _this.closest('.file-container').find('input#thumbnail-value').val(url);
                }
            });

        }
    })

    $('.file-container input[type=file]').on('change',function () {
        var file = $(this);
        var fileInput = $(this).closest('.file-container').find('input[name=thumbnail-value]');
        var form = fileInput.closest('form');
        var formData = new FormData(form[0]);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: "/upload-file",
            data: formData,
            dataType: 'json',
            async: false,
            processData: false,
            contentType: false,
            success: function (response) {
                if(response.filePath){
                    file.closest('.file-container').find('img').attr('src',response.filePath);
                    fileInput.val(response.filePath);
                } else {
                    alert(response.error);
                }
            },
        });
    });


    $(document).on('submit','form',function (e) {
        return ValidateForm(e);
    });

    $(document).on('submit','form.add-to-cart',function (e) {

        var form = $(this);
        var formData = new FormData(form[0]);
        var btn = form.find('button');
        var text = btn.text();
        var items_cont = $('.cart-dropdown-item .items').eq(0);
        var items_html = '';

        var url = form.attr('action');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            async: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
                btn.append(' <i class="fas fa-spinner"></i>');
            },
            success: function (response) {
                if(response.success){
                    btn.find('.fa-spinner').remove();
                    btn.append('<i class="fa fa-check"></i>');

                    $.each(response.items, function(k, item) {
                        items_html += '<div class="row">\n' +
                            '<div class="col-sm-7 no-padd">\n' +
                            item.product.title.substring(0,20)+'...' + '\n' +
                            '</div>\n' +
                            '<div class="col-sm-2">\n' +
                            item.qty + '\n' +
                            '</div>\n' +
                            '<div class="col-sm-3 no-padd">\n' +
                            item.product.price.toFixed(2) + response.currency + '\n' +
                            '</div>\n' +
                            '</div>'
                    });

                    items_cont.html(items_html);
                    items_cont.closest('.items-cont').find('span.cart-total').eq(0).text(response.total.toFixed(2) + response.currency)

                    setTimeout(function () {
                        btn.find('.fa-check').remove();
                        $('.navbar-nav .items-count').eq(0).text('(' + response.items_number + ')')
                    },2000);
                } else {
                    btn.find('.fa-spinner').remove();
                    form.append('<p class="error">'+response.message+'</p>');
                    setTimeout(function () {
                        form.find('p.error').remove();
                    },2000);
                }
            },
            error: function(){
                alert('failure');
            }
        });

        e.preventDefault();

        return false;
    })
});