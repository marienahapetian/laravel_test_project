$(document).ready(function () {
    if( $('#post-content').length ){
        var contentEditor = CKEDITOR.replace('post-content');
    }

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('change', function () {
            $('#' + CKEDITOR.instances[i].name).eq(0).html(CKEDITOR.instances[i].getData())
        });
    }

})