<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $statuses = array(
            '1'=>'Incomplete',
            '2'=>'Pending',
            '3'=>'Processed',
            '4'=>'Shipped',
            '5'=>'Cancelled',
            '6'=>'Return',
            '7'=>'Partially Shipped',
            '8'=>'Shipping',
            '9'=>'Partially Returned'
        );
        foreach($statuses as $code=>$status){
            DB::table('order_status')->insert([
                'name'=>$status
            ]);
        }
    }
}
