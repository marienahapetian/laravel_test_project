<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['key'=>'currency','value'=>'$','help_text'=>'Currency of the products'],
            ['key'=>'language','value'=>'en','help_text'=>'Language of the website'],
            ['key'=>'library-token','value'=>'','help_text'=>'Access Token for Shortly App'],
            ['key'=>'shortly-token','value'=>'','help_text'=>'Access Token for Shortly App'],
        ];
        foreach($settings as $setting){
            DB::table('settings')->insert([
                'key' => $setting['key'],
                'value' => $setting['value'],
                'help_text'=> $setting['help_text']
            ]);
        }
    }
}
