<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->text(),
        'content' => $faker->text(),
        'thumbnail' => $faker->imageUrl(),
        'price' => $faker->numberBetween(1,100),
        'author_id' => 1,
    ];
});
