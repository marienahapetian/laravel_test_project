<?php

return [
    'currencies' => [
        'usd'=>'$',
        'euro'=>'€'
    ],
    'languages' => [
        'en'=>'english',
        'ru'=>'russian'
    ],
];
